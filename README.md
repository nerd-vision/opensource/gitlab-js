# [@nerdvision/gitlab-js](https://www.npmjs.com/package/@nerdvision/gitlab-js)

[![Join us on Discord](https://img.shields.io/discord/588772895870943302?label=join%20us%20on%20discord&logo=discord&style=for-the-badge)](https://discord.gg/TxPG97U)

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

A more intuitive and modern way to interact with the GitLab API in JavaScript. This library aims to abstract out the GitLab API
and unify its usage across common use cases. It also aims to fully support types for all arguments and returned data.

> ***This library is in alpha and there may be breaking changes & parts that are not functional***. It is not feature complete yet and is missing many of the [GitLab API routes](https://docs.gitlab.com/ee/api/api_resources.html).
>
> Need some functionality that we haven't implemented yet? [Make an issue](https://gitlab.com/nerd-vision/opensource/gitlab-js/-/issues/new) or implement it yourself and [make a merge request!](#contributing)

## Installation & set up

Install the package:

```bash
npm i @nerdvision/gitlab-js
```

Initialise it with [your GitLab token](https://gitlab.com/-/profile/personal_access_tokens) or without anything to use the API
anonymously:

```javascript
import {Client} from '@nerdvision/gitlab-js';

const client = new Client();
const authedClient = new Client('your-token-here');
```

Using a privately hosted version of GitLab? Just specify the host in the config:

```javascript
const client = new Client({
  token: 'your-token-here',
  host: 'https://gitlab.mysite.com',
});
```

Don't want to specify the arguments in code? The library defaults to the value of `GITLAB_TOKEN` and `GITLAB_HOST` if you don't
pass in an option for them.

Now you can start using the library!

```javascript
const projects = await client.projects.list();
```

## General usage

Everything in the project comes from the base `client` we've initialised above. When you request something, the context is kept so
you only need to query for the new data.

E.G. if you wanted to grab all projects inside the group `nerd-vision/opensource`, you could use the following code:

```javascript
const group = await client.groups.find('nerd-vision/opensource');
const projects = await group.projects.list();
```

Cool! We've got a list of projects in that group. Let's now grab all the open issues in the first project in the list:

```javascript
const project = projects[0];
const issues = await project.issues.list({state: 'opened'});
```

We've now very easily pulled a list of all open issues in the first project in the `nerd-vision/opensource` group!

Most available APIs in this project inherit one of the following classes, ensuring a consistent developing experience across all
of your GitLab API usage.

### Collections

Collections of items handle access routes that return multiple of the same item. There are a few options available to you for
accessing collections. All of these methods return promises.

* `.list(query = {})`

Query GitLab for a list of items that match the optional query.

Example:

```javascript
const allIssues = await project.issues.list();
const openIssues = await project.issues.list({state: 'opened'});
```

* `.find(id: string | number, query = {})`

Find a single item in the list by its numerical id or slug. You can also specify an optional query to only return the item if it
fulfills the given query.

Example:

```javascript
const groupFromId = await client.groups.find(10697584);
const groupFromSlug = await client.groups.find('nerd-vision/opensource');
```

* `.save(item)`

Save the given item into the collection. This can either be an existing instance of the item, or an object with the data that
you're looking to save. If the item has an ID (or iid), it will attempt to send an update request, else it will attempt to save a
new instance

Example:

```javascript
await client.groups.save({name: 'my-new-group'}); // creates a new group
await client.groups.save({id: 123, name: 'my-new-group'}); // updates an existing group with the id 123
```

### Items

Items store data and allow you to access relations of that item.

* `.data`

Access the data for the item. This is currently the raw response from GitLab.

### Singular items

Singular items are items that do not live inside collections. E.G. A pipeline job can only ever have one trace associated with it,
so trace is a singular item instead of a collection of traces.

## Available APIs

### Key

* **C** - Collection
* **I** - Item
* **S** - Singular item
* **M** - Callable method to do said action

### API tree

* client
  * groups - **C**
  * projects - **C**
  * issues - **C**
  * mergeRequests - **C**
  * epics - **C**
  * users - **C**

### Links between endpoints

* group - **I**
  * projects - **C**
  * epics - **C**
  * boards - **C**
  * milestones - **C**
  * issues - **C**
  * mergeRequests - **C**

* project - **I**
  * issues - **C**
  * mergeRequests - **C**
  * boards - **C**
  * branches - **C**
  * tags - **C**
  * pipelines - **C**
  * jobs - **C**
  * releases - **C**
  * commits - **C**

* mergeRequest - **I**
  * toggleDraft - **M**
  * approve - **M**
  * unapprove - **M**
  * approvals - **S**
  * discussions - **C**
  * notes - **C**

* issue - **I**
  * discussions - **C**
  * notes - **C**

* epic - **I**
  * issues - **C**
  * discussions - **C**
  * notes - **C**

* job - **I**
  * trace - **M**

* pipeline - **I**
  * jobs - **C**

## Types

The library was built with Typescript and includes types for all the method usages and implemented APIs. Return/query parameters
are not fully implemented yet.

## Need some help?

If you need some help with the library you can [make an issue](https://gitlab.com/nerd-vision/opensource/gitlab-js/-/issues/new)
or [join us on Discord](https://discord.gg/TxPG97U) and we'll be happy to assist!

## Contributing

Many [GitLab APIs](https://docs.gitlab.com/ee/api/api_resources.html) are still missing from the library. If you're looking for
something simple to get started with, this is a great place!

Merge requests are welcome. For major changes,
please [open an issue](https://gitlab.com/nerd-vision/opensource/gitlab-js/-/issues/new) first to discuss what you would like to
change.

If you're looking for something else to contribute, take a look
through [our open issues](https://gitlab.com/nerd-vision/opensource/gitlab-js/-/issues).

Please make sure to update tests as appropriate.

## Projects that use @nerdvision/gitlab-js

* https://gitlab.com/himinds-pub/wiki-example ([blog post about how it works](https://medium.com/geekculture/lets-take-some-baby-steps-creating-a-pipeline-status-report-using-the-gitlab-api-69920294cc0) ([auch auf deutsch](https://ichi.pro/de/machen-wir-ein-paar-kleine-schritte-erstellen-eines-pipeline-statusberichts-mit-der-gitlab-api-7254739340480)))

Using us in your project and want to be featured here? [Create an issue](https://gitlab.com/nerd-vision/opensource/gitlab-js/-/issues/new) with a link to your repository and any other information you think is relevant!

## License

[Apache 2.0](https://choosealicense.com/licenses/apache-2.0/)
