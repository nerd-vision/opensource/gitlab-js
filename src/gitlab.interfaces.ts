export interface IGenericItem {
  id: number;
  iid: number;
  created_at: Date;
  updated_at: Date;
}

export interface IProject extends IGenericItem {
  description: string;
  default_branch: string;
  ssh_url_to_repo: string;
  http_url_to_repo: string;
  web_url: string;
  readme_url: string;
  tag_list: string[];
  name: string;
  name_with_namespace: string;
  path: string;
  path_with_namespace: string;
  last_activity_at: Date;
  forks_count: number;
  avatar_url: string;
  start_count: number;
  visibility?: 'private' | 'public';
  owner?: {
    id: number;
    name: string;
    created_at: Date;
  };
  issues_enabled?: boolean;
  open_issues_count?: number;
  merge_requests_enabled?: boolean;
  jobs_enabled?: boolean;
  wiki_enabled?: boolean;
  snippets_enabled?: boolean;
  can_create_merge_request_in?: boolean;
  resolve_outdated_diff_discussions?: boolean;
  container_registry_enabled?: boolean;
  creator_id?: number;
  namespace?: {
    id: 3;
    name: string;
    path: string;
    kind: string;
    full_path: string;
  };
  import_status?: string;
  archived?: boolean;
  shared_runners_enabled?: boolean;
  star_count?: number;
  runners_token?: string;
  ci_default_git_depth?: number;
  ci_forward_deployment_enabled?: boolean;
  public_jobs?: boolean;
  shared_with_groups?: any[];
  only_allow_merge_if_pipeline_succeeds?: boolean;
  allow_merge_on_skipped_pipeline?: boolean;
  only_allow_merge_if_all_discussions_are_resolved?: boolean;
  remove_source_branch_after_merge?: boolean;
  request_access_enabled?: boolean;
  merge_method?: string;
  autoclose_referenced_issues?: boolean;
  suggestion_commit_message?: any;
  /**
   * @deprecated - use marked_for_deletion_on instead
   */
  marked_for_deletion_at?: Date;
  marked_for_deletion_on?: Date;
  statistics?: {
    commit_count: number;
    storage_size: number;
    repository_size: number;
    wiki_size: number;
    lfs_objects_size: number;
    job_artifacts_size: number;
    packages_size: number;
    snippets_size: number;
  };
  _links?: {
    self: string;
    issues: string;
    merge_requests: string;
    repo_branches: string;
    labels: string;
    events: string;
    members: string;
  };
}
